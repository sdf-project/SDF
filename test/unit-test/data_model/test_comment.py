import unittest
from xml.etree import ElementTree
from xml.etree.ElementTree import Element

from SDF.data_model.comment import Comment


class TestComment(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.valid_element_abc = ElementTree.fromstring(
            """
    <comment>
        abc
    </comment>
            """
        )
        cls.valid_element_indented = ElementTree.fromstring(
            """
    <comment>
        - abc
        - def
          - ghi
        - jkl
    </comment>
            """
        )
        cls.valid_element_abc_def = ElementTree.fromstring(
            """
    <comment>
        abc  def
    </comment>
            """
        )
        cls.invalid_element_tag = ElementTree.fromstring(
            """
    <commen>
        abc
    </commen>
            """
        )
        cls.invalid_element_attrib = ElementTree.fromstring(
            """
    <comment attr="def">
        abc
    </comment>
            """
        )

    def test_init(self):
        # invalid arguments
        with self.assertRaises(TypeError, msg="comment is not optional"):
            Comment()
        with self.assertRaises(ValueError, msg="comment cannot be empty"):
            Comment("")
        with self.assertRaises(ValueError, msg="whitespace-only comment is empty after stripping whitespace"):
            Comment(" \n ")
        with self.assertRaises(TypeError, msg="comment must be str"):
            Comment(b"abc")
        with self.assertRaises(TypeError, msg="comment must be str"):
            Comment(["a", "b", "c"])
        with self.assertRaises(TypeError, msg="comment must be str"):
            Comment(1)

        # comment sanitization
        self.assertEqual(Comment("abc").comment, "abc")
        self.assertEqual(Comment("abc def").comment, "abc def")
        self.assertEqual(Comment(" \n ab c  \n de \n\n ").comment, "ab c\nde")

    def test_property_comment(self):
        comment = Comment("abc")
        self.assertEqual(comment.comment, "abc")
        comment.comment = "  abc def "
        self.assertEqual(comment.comment, "abc def")
        comment.comment = " \n ab c  \n de \n\n "
        self.assertEqual(comment.comment, "ab c\nde")

    def test_to_xml_element(self):
        # simple case
        text = "abc def"
        element = Comment(text).to_xml_element()
        self.assertEqual(len(element.attrib), 0, msg="<comment> elements have no attributes")
        self.assertEqual(element.text, text, msg="input and output should be equal")
        self.assertEqual(element.tag, "comment", msg="<comment> tag expected")
        self.assertEqual(len(element), 0, msg="element must have no children")

    def test_whitespace_trimming(self):
        text = " \n  abc \n  def\n    - ghi \n"
        element = Comment(text).to_xml_element()
        self.assertEqual(element.text, "abc\ndef\n  - ghi", msg="unindent and remove blank lines")

    def test_escape_unescape(self):
        text = "<a><b></a></b>"  # invalid XML, so the parser will fail if it is not escaped properly
        in_element = Comment(text).to_xml_element()  # save
        out_element = ElementTree.fromstring(ElementTree.tostring(in_element))  # parse
        self.assertEqual(Comment.from_xml_element(out_element).comment, text)

    def test_from_xml_element(self):
        with self.assertRaises(ValueError):
            Comment.from_xml_element(self.invalid_element_tag)
        with self.assertRaises(ValueError):
            Comment.from_xml_element(self.invalid_element_attrib)
        self.assertEqual(Comment.from_xml_element(self.valid_element_abc).comment, "abc")
        self.assertEqual(Comment.from_xml_element(self.valid_element_abc_def).comment, "abc  def")
        self.assertEqual(Comment.from_xml_element(self.valid_element_indented).comment, "- abc\n- def\n  - ghi\n- jkl")

    def test_eq(self):
        self.assertEqual(Comment("abc"), Comment("abc"))
        self.assertNotEqual(Comment("abc"), Comment("ab c"))
        self.assertNotEqual(Comment("ab\nc"), Comment("ab c"))
        self.assertNotEqual(Comment("ab\nc"), Comment("abc"))
        self.assertEqual(Comment(" abc  \n d \n"), Comment("abc\nd"))
        self.assertNotEqual(Comment("abc"), "abc")

    def test_repr(self):
        self.assertEqual(eval(Comment("abc").__repr__()), Comment("abc"))
