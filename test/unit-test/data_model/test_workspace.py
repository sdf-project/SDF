from datetime import datetime
from io import StringIO
import unittest
from PIL import Image
import numpy as np
from SDF.data_model import Workspace, ImageDataset, ArrayDataset1D
from SDF.file_io import write_sdf, load_from_sdf


class TestWorkspace(unittest.TestCase):
    def test_workspace(self):
        sdf = Workspace(name="my experiment", owner="me", date=datetime.now())
        sdf.comment = "comment on the experiment"
        sdf.samples["my first sample"] = "sample description"
        sdf.samples["my second sample"] = "sample description"

        measurement_1 = Workspace("my first measurement")
        sdf.workspaces.add(measurement_1)
        measurement_1.comment = "comment on my first measurement:\n" \
                                "  - it consists of two images\n" \
                                "  - they are random, but beautiful!"
        measurement_1.instruments["my microscope"] = dict(cost=(9001, "EUR"), color="black")
        image_1 = Image.fromarray(np.random.randint(0, 255, (10, 10), dtype=np.uint8))  # random 10x10 image
        image_2 = Image.fromarray(np.random.randint(0, 255, (10, 10), dtype=np.uint8))
        measurement_1.datasets.add(ImageDataset("image 1", image_1))
        measurement_1["image 1"].parameters["zoom-level"] = 30
        measurement_1["image 1"].parameters["wavelength"] = 509, "nm"
        measurement_1.datasets.add(ImageDataset("image 2", image_2))

        measurement_2 = Workspace("my second measurement")
        sdf.workspaces.add(measurement_2)
        measurement_2.comment = "comment on my first measurement"
        x = np.arange(100)
        y = np.random.randint(0, 10, 100)
        measurement_2.datasets.add(ArrayDataset1D("x", x))
        measurement_2.datasets.add(ArrayDataset1D("y", y))

        sdf_file = StringIO()
        write_sdf(sdf, sdf_file)
        sdf_file.seek(0)
        self.assertEqual(sdf, load_from_sdf(sdf_file))
