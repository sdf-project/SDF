from setuptools import setup, find_packages

setup(
    name='sdf',
    version='0.28',
    description='Python Interface of a Standard Data Format',
    url='',
    author='Burkhard Geil, Filip Savic, Ilyas Kuhlemann, Niklas Mertsch',
    author_email='bgeil@gwdg.de',
    maintainer_email='niklas.mertsch@stud.uni-goettingen.de',
    license='GNU GPLv3',
    python_requires='>=3.6',
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "sdf-convert=SDF.CLI.convert:main",
            "sdf-update=SDF.CLI.update:main",
        ],
        "gui_scripts": [
            "sdf-browser = SDF.GUI.browser.main:main",
            "sdf-convert-gui = SDF.GUI.converter.sdf_converter:main",
        ]
    },
    install_requires=[
        'numpy',
        "Pillow",
        "scipy",
        "PyQt5",
        "matplotlib",
        "dataclasses; python_version < '3.7'",
        "importlib-metadata; python_version < '3.8'"
    ],
    zip_safe=False
)
