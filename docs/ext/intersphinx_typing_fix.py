"""
Intersphinx looks for references in the objects.inv file provided by external documentation like docs.python.org/3.
These references are matched by (domain, role, targetname), the objects.inv file then provides a URL.
For the typing module, type annotations are read by intersphinx as role 'class', but some classes like Any or Tuple
are declared as 'data' in the objects.inv file.

This extension provides role translation for this case.

Problem description: https://bugs.python.org/issue31024
Code inspriration: https://github.com/Czaki/sphinx-qt-documentation
"""
from sphinx.application import Sphinx
from sphinx.environment import BuildEnvironment
from docutils.nodes import Element, TextElement
from docutils import nodes
from typing import Optional, Dict, Any
from sphinx.ext.intersphinx import InventoryAdapter


def missing_reference(
    app: Sphinx, env: BuildEnvironment, node: Element, contnode: TextElement
) -> Optional[nodes.reference]:
    if node.get("refdomain") != "py":
        return None
    role: str = node.get("reftype")
    if role != "class":
        return None
    reftarget: str = node.get("reftarget")
    if not reftarget or ("." in reftarget and not reftarget.startswith("typing.")):
        return None

    # search for proper reference in inventory
    inventories = InventoryAdapter(env)
    target_dicts = [
        inventories.named_inventory["python"]["py:class"],
        inventories.named_inventory["python"]["py:data"],
    ]
    target_names = [reftarget, f"typing.{reftarget}"]

    # order is important: best to worst fit
    found = False
    proj, version, uri, dispname = "", "", "", ""
    for target_name in target_names:
        for target_dict in target_dicts:
            if not found and target_name in target_dict.keys():
                proj, version, uri, dispname = target_dict[target_name]
                if "typing.html#typing." in uri:
                    found = True
                if dispname == "-":
                    dispname = target_name
    if not found:
        return None

    # return proper node
    newnode = nodes.reference("", "", internal=False, refuri=uri)
    newnode.append(contnode.__class__(dispname, dispname))
    return newnode


def setup(app: Sphinx) -> Dict[str, Any]:
    app.setup_extension("sphinx.ext.intersphinx")
    app.connect("missing-reference", missing_reference)
    return {"version": "0.1", "env_version": 1, "parallel_read_safe": True}
