Tests
*****

The SDF library sadly is not fully covered by tests, but there are some tests available.

Run all Tests
=============

The ``Makefile`` in the top-level ``test`` directory is intended to run all implemented tests. There should be a target
for each type of implemented test.

Unittest
========

The ``test/unit-test`` directory contains test cases written for the :py:mod:`unittest` module, which is part of
the Python standard library. To execute all tests in this directory, run ``python -m unittest`` in the ``test``
directory, or use the ``unittest`` target.

.. note::

    The directory cannot be named ``unittest``, because then ``python -m unittest`` would try to execute that directory
    instead of the :py:mod:`unittest` module.

Testing ``sdf-convert``
=======================

The target ``sdf-convert`` tests the CLI conversion tool.

Doctest
=======

Many of the code examples in the documentation are implemented as
`doctest <https://www.sphinx-doc.org/en/master/usage/extensions/doctest.html>`_ directives.
These tests can be executed via ``make test``, using the ``Makefile`` in the ``doc`` directory. The target ``doctest``
of the ``test`` Makefile redirects there.
