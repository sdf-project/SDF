Command Line Interface (CLI)
****************************

sdf-convert
-----------

The tool :code:`sdf-convert` converts files to the SDF format. Use :code:`sdf-convert --help` for details and examples:

.. runcmd:: sdf-convert --help

sdf-update
----------

The tool :code:`sdf-update` updates the current SDF installation by downloading the zip file from GitLab, uninstalling
the current SDF installation and then installing the downloaded version. Use :code:`sdf-update --help` for details:

.. runcmd:: sdf-update --help
