Graphical User Interface
************************

sdf-convert-gui
---------------

:code:`sdf-convert-gui` opens a graphical converter with the same functionality as :code:`sdf-convert`.

.. figure:: /img/screenshot-sdf-convert-gui.png
   :alt: Screenshot of the Converter GUI

   Screenshot of the Converter GUI

sdf-browser
-----------

:code:`sdf-browser` opens a graphical SDF file browser. It can be used to display the content of SDF files.

.. figure:: /img/screenshot-sdf-browser.png
   :alt: Screenshot of the SDF Browser

   Screenshot of the SDF Browser
