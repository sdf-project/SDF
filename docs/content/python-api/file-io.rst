SDF.file_io: Read and write files
*********************************

The submodule :py:mod:`SDF.file_io` contains the functionality for reading files to SDF objects and writing those to
files.

.. py:currentmodule:: SDF.file_io

Read
====

To read files to SDF objects, there are multiple functions, one per implemented input format:

- :py:func:`~load_from_sdf` (filename): read .sdf files
- :py:func:`~load_from_jpk` (filename): read .jpk files
- :py:func:`~load_from_mfp` (filename): read .ibw files (from MFP-3D instruments)
- :py:func:`~load_from_lsm` (filename): read .lsm files
- :py:func:`~load_from_oif` (filename): read .oif files
- :py:func:`~load_from_oib` (filename): read .oib files

They all expect a filename as argument and return an SDF Object (usually a Workspace).

Write
=====

SDF Objects (Workspace and Dataset types) can be written to files by calling
:py:func:`~write_sdf` (sdf_object, filename).

This calls :py:meth:`XMLWritable.to_xml_element <SDF.data_model.abstract.XMLWritable.to_xml_element>` () and serializes
that XML Element to a text file. This could also be done by any other XML library.

Example
=======

.. code-block:: python3

    from SDF.file_io import load_from_lsm, write_sdf

    sdf = load_from_lsm("my-data.lsm")
    write_sdf(sdf, "my-data.sdf")
