from abc import abstractmethod, ABC
from xml.etree.ElementTree import Element


class XMLWritable(ABC):
    """Abstract class for all SDF objects that are directly mappable to XML elements"""

    @abstractmethod
    def to_xml_element(self) -> Element:
        """Returns an Element object that contains all information needed to export this object as XML."""
        pass

    @classmethod
    @abstractmethod
    def from_xml_element(cls, element: Element) -> "XMLWritable":
        """Generates an instance of this class from an Element"""
        pass


class Data(XMLWritable):
    """Abstract class for all SDF objects representing <data> elements"""

    @property
    @abstractmethod
    def type_for_xml(self) -> str:
        """The 'type' attribute the wrapping <dataset> element has to have"""
        pass
