from xml.etree.ElementTree import Element

from SDF.data_model._helper_functions import pop_element_text, element_is_empty
from SDF.data_model.abstract import XMLWritable


class Owner(XMLWritable):
    """Represents an SDF <owner> element"""

    def __init__(self, name: str):
        if not isinstance(name, str):
            raise TypeError(f"Expected a string, got {type(name)}")
        self.name = " ".join(s for s in name.strip().split())

    def to_xml_element(self) -> Element:
        element = Element("owner")
        element.text = self.name
        return element

    @classmethod
    def from_xml_element(cls, element: Element) -> "Owner":
        if element.tag != "owner":
            raise ValueError(f"Expected an <owner> element, got {element.tag}")

        text = pop_element_text(element)

        if not element_is_empty(element):
            raise ValueError("Element is not empty")

        return cls(text)

    def __repr__(self):
        return f"{self.__class__.__name__}({self.name!r})"

    def __eq__(self, other):
        if isinstance(other, Owner):
            return self.name == other.name
        return False
