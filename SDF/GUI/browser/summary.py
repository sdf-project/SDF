"""Widget to display a few details of any sdf_object."""
from typing import Tuple, Iterable, Optional

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QVBoxLayout, QHBoxLayout

from SDF.GUI.browser.parameter_popup import ParameterPopup
from SDF.data_model import ParameterType, ParameterSet, SDFObject, Parameter, ArrayDataset1D


class SummaryWidget(QWidget):
    """
    Widget to display a few details of any sdf_object.
    """

    def __init__(self):
        super().__init__()

        self.sdf_obj: Optional[SDFObject] = None
        self.par_popups = []

        self.name_textbox = QLabel("")

        self.parameters_textbox = QLabel("")
        self.parameters_button = QPushButton("Parameters")

        self.instruments_textbox = QLabel("")
        self.instruments_button = QPushButton("Instruments")

        self.samples_textbox = QLabel("")
        self.samples_button = QPushButton("Samples")

        self.init_ui()

    def init_ui(self):
        """
        Initializes all widgets (buttons, labels, etc.) displayed
        on this widget.
        """
        self.setLayout(QVBoxLayout())

        # row: name
        hbox_name = QHBoxLayout()
        hbox_name.addWidget(QLabel("Name:"))
        hbox_name.addWidget(self.name_textbox)
        hbox_name.addStretch(1)
        self.layout().addLayout(hbox_name)

        hbox_parameters = QHBoxLayout()
        hbox_parameters.addWidget(self.parameters_button)
        hbox_parameters.addWidget(QLabel(":"))
        hbox_parameters.addWidget(self.parameters_textbox)
        hbox_parameters.addStretch(1)
        self.layout().addLayout(hbox_parameters)

        # row: instruments and samples
        hbox_instruments_and_samples = QHBoxLayout()
        hbox_instruments_and_samples.addWidget(self.instruments_button)
        hbox_instruments_and_samples.addWidget(QLabel(":"))
        hbox_instruments_and_samples.addWidget(self.instruments_textbox)
        hbox_instruments_and_samples.addWidget(self.samples_button)
        hbox_instruments_and_samples.addWidget(QLabel(":"))
        hbox_instruments_and_samples.addWidget(self.samples_textbox)
        hbox_instruments_and_samples.addStretch(1)
        self.layout().addLayout(hbox_instruments_and_samples)

        self.layout().addStretch(1)

        # buttons
        self.parameters_button.setDisabled(True)
        self.parameters_button.clicked.connect(self._popup_parameter_widget)
        self.instruments_button.setDisabled(True)
        self.instruments_button.clicked.connect(self._popup_instrument_widget)
        self.samples_button.setDisabled(True)
        self.samples_button.clicked.connect(self._popup_sample_widget)

    def display_sdf_object(self, sdf_obj: SDFObject):
        """
        Updates all labels and texts to show information on the given sdf_object.
        """
        self.sdf_obj = sdf_obj
        label = sdf_obj.name
        if isinstance(sdf_obj, ArrayDataset1D) and sdf_obj.unit is not None:
            label += f" (unit: '{sdf_obj.unit}')"
        self.name_textbox.setText(label)

        # get number of parameters:
        n_par, n_blocks, n_par_in_blocks = self.get_number_of_parameters(sdf_obj.parameters)

        param_str = f"<b>{n_par}</b> parameters, "
        param_str += f"<b>{n_blocks}</b> parameter-blocks, "
        param_str += f"<b>{n_par_in_blocks}</b> parameters in total"
        self.parameters_textbox.setText(param_str)

        n_instruments = len(sdf_obj.instruments)
        self.instruments_textbox.setText(f"<b>{n_instruments}</b> instruments")

        n_samples = len(sdf_obj.samples)
        self.samples_textbox.setText(f"<b>{n_samples}</b> samples")

        self.samples_button.setDisabled(n_samples == 0)
        self.instruments_button.setDisabled(n_instruments == 0)
        self.parameters_button.setDisabled(n_par_in_blocks == 0)

    def get_number_of_parameters(self, par: Iterable[ParameterType]) -> Tuple[int, int, int]:
        """
        Method to return the number of parameters in given list of parameters.
        Number of parameters are returned separated in number of top-level
        parameters, blocks, and parameters in blocks and lower levels.

        :return: Tuple of 3 integers:

          1. Number of top-level parameters
          2. Number of parameter blocks
          3. Number of parameters in blocks
        """
        n_par = 0
        n_blocks = 0
        n_par_in_blocks = 0
        for p in par:
            if isinstance(p, ParameterSet):
                n_blocks += 1
                _n = self.get_number_of_parameters(p)
                n_par_in_blocks = n_par_in_blocks + _n[0] + _n[2]
            else:
                n_par += 1
        return n_par, n_blocks, n_par_in_blocks

    def _popup_parameter_widget(self):
        ParameterPopup(parent=self, title=f"Parameters of {self.sdf_obj.name}",
                       parameters=self.sdf_obj.parameters).show()

    def _popup_instrument_widget(self):
        instruments = [ParameterSet(inst.name, inst) for inst in self.sdf_obj.instruments]
        ParameterPopup(parent=self, title=f"Instruments of {self.sdf_obj.name}",
                       parameters=instruments).show()

    def _popup_sample_widget(self):
        samples = [Parameter(sample.name, value=sample.comment) for sample in self.sdf_obj.samples]
        ParameterPopup(parent=self, title=f"Samples of {self.sdf_obj.name}",
                       parameters=samples).show()
