from typing import Optional, Iterable

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QTreeWidget, QWidget, QTreeWidgetItem, QHBoxLayout

from SDF.data_model import ParameterType, ParameterSet, Parameter


class ParameterPopup(QWidget):
    def __init__(self, parent: QWidget, title: str, parameters: Iterable[ParameterType]):
        # Qt.Window in combination with a parent makes this widget a secondary window.
        # Secondary windows are closed when the parent is closed
        # (see https://doc.qt.io/qt-5/application-windows.html#primary-and-secondary-windows)
        # QTreeWidget is no QWidget subclass and thus cannot take this flag, so this class is a QWidget with a
        # QTreeWidget as its only content
        super().__init__(parent, Qt.Window)
        self.setLayout(QHBoxLayout())
        self.setWindowTitle(title)

        self.treewidget = QTreeWidget()
        self.treewidget.setColumnCount(2)
        self.treewidget.setHeaderLabels(["Name", "Value"])
        self.layout().addWidget(self.treewidget)

        for par in parameters:
            self.treewidget.addTopLevelItem(self.__par_to_item(par, parent=None))

    @staticmethod
    def __par_to_item(par: ParameterType, parent: Optional[QTreeWidgetItem]):
        if isinstance(par, ParameterSet):
            item = QTreeWidgetItem(parent, [par.name, ""])
            for child in par:
                item.addChild(ParameterPopup.__par_to_item(child, parent=item))
        else:
            value_str = par.value
            if par.unit is not None:
                value_str += f" (unit: '{par.unit}')"
            item = QTreeWidgetItem(parent, [par.name, value_str])

        return item
