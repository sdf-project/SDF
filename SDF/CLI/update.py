import argparse
import subprocess
import sys


def main():
    parser = argparse.ArgumentParser(description="Update the SDF installation")
    target_group = parser.add_mutually_exclusive_group()
    target_variable = "version"
    target_group.add_argument("-b", "--branch", dest=target_variable, help="Branch to install (default: stable)",
                              default="stable")
    target_group.add_argument("-c", "--commit", dest=target_variable, help="Commit to install (full SHA hash)")
    target = getattr(parser.parse_args(), target_variable)

    zip_url = f"https://gitlab.gwdg.de/sdf-project/SDF/-/archive/{target}/SDF-{target}.zip"

    # pip or pip3? conda or system pip? Run the pip module explicitly from the running python executable to handle that
    pip_exe = f"{sys.executable} -m pip"

    # --no-cache-dir: ensure the zip file is re-downloaded
    # --upgrade: ensure actually installing the requested SDF version, even if the version number is still the same
    install_cmd = f"{pip_exe} install --no-cache-dir --upgrade {zip_url}"

    # Reinstalling SDF means removing this file while it is executed. This is bound to cause problems.
    # This will run the command in a new process and immediately exit this process, closing the file pointer.

    # Forking the process means returning to the console immediately, which might (and should) confuse users.
    #  To hide this,
    #  1. overwrite the prompt by printing a carriage return ('\\r') to the terminal
    #  2. ask the user to press ENTER after the installation
    # TODO: find an elegant way to not confuse the user
    carriage_return = f"{sys.executable} -c \"print('\\r', end='')\""
    press_enter = f"{sys.executable} -c \"print('Press ENTER to finish the update', end='')\""
    install_cmd = "&&".join([carriage_return, install_cmd, press_enter])

    # run the actual command
    subprocess.Popen(install_cmd, shell=True, start_new_session=True)
